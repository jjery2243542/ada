#include <cstdio>
#include <algorithm>
#include <vector>
#include <functional>

typedef struct box
{
	long long int x, y;
	box()
	{
		x = 0;
		y = 0;
	}
	bool operator > (const struct box &b) const
	{
		if (x == b.x)
			return (y > b.y);
		else 
			return (x > b.x);
	}//for sort
	bool operator ==(const struct box &b) const
	{
		return (x == b.x && y == b.y);
	}
}box;

long long int combine(box A[], box B[], int a, int b) 
{
	std::vector<box> buf(a + b);// buffer
	int i = 0, j = 0, n = 0;
	long long sum = 0;
	while (i < a && j < b) {
		if (A[i].y > B[j].y) {
			buf[n] = A[i];
			sum += b - j;
			i++; n++;
		}// from B[j] to B[b - 1] could put into A[i]
		else if (A[i].y == B[j].y) {
			if (A[i].x > B[j].x) { 
				buf[n] = A[i];
				sum += b - j;
				i++; n++;
			}// from B[j] to B[b - 1] could put into A[i]
			else if (A[i].x == B[j].x) {
				long long int countA = 0, countB = 0;
				box tmp;
				tmp.x = A[i].x; tmp.y = A[i].y;
				for (int k = j; B[k] == tmp && k < b; k++)
					countB++;// find out how many same boxes in B
				for (int k = i; A[k] == tmp && k < a; k++)
					countA++;// find out how many same boxes in A
				sum += (b- j + countB) * countA;// add to sum
				for (int k = 0; k < countA + countB; k++, n++)
					buf[n] = tmp;
				i += countA; j += countB;
			}
		}
		else {	
			buf[n] = B[j];
			j++; n++;
		}// means that B[j] couldn't put into A[i]
	}
	if (j == b) { 
		for (int k = i; k < a; k++, n++)
			buf[n] = A[k];
	}
	else {
		for (int k = j; k < b; k++, n++)
			buf[n] = B[k];
	}// put remaining boxes into buffer
	for (int i = 0; i < a + b; i++) 
		A[i] = buf[i];// copy back to the array
	return sum;
}

long long int mergesort(box boxes[], int n) {
	if (n == 1) 
		return 0;
	long long int a, b, c;
	if (n % 2 == 0) {
		a = mergesort(boxes, n/2);
		b = mergesort(boxes + n/2, n/2);// recusive call
		c = combine(boxes, boxes + n/2, n/2, n/2);// comine two array and count the answer
	}
	else {
		a = mergesort(boxes, n/2);
		b = mergesort(boxes + n/2, n/2 + 1);
		c = combine(boxes, boxes + n/2, n/2, n/2 + 1);// same
	}
	return a + b + c;
}

int main(void)
{
	int T, n;
	int buf[2];
	scanf("%d", &T);
	for (int i = 0; i < T; i++) {
		scanf("%d", &n);
		box boxes[100000];
		for (int j = 0; j < n; j++) {
			scanf("%d%d", &buf[0], &buf[1]);
			if (buf[0] < buf[1]) {
				boxes[j].x = buf[1];
				boxes[j].y = buf[0];
			}
			else {
				boxes[j].x = buf[0];
				boxes[j].y = buf[1];
			}// let y <= x
		}
		std::sort(boxes, boxes + n, std::greater<box>());// sort in decending order
		printf("%lld\n", mergesort(boxes, n));
	}
	return 0;
}
