#include <stdio.h>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

typedef struct edge
{
	int vertex;
	int weight;
	bool can_take(int ans, int i)// the ith bit(from the left)
	{
		return (((weight | ans) >> i) == (ans >> i));
	}
}edge;

int find_leftmost_bit(int n) 
{
	for (int i = 30; i >= 0; i--) {
		if ((1 << i) < n) 
			return i;
	}
}

//return how many vertex are connected to 0 under the condition
int BFS(vector<vector<edge> > &adj_list, int V, int ans, int i)
{ 
	int vertex, num_connected_vertex = 1;
	bool visit[V];
	queue<int> Q;
	for (int i = 0; i < V; i++)
		visit[i] = 0;
	visit[0] = 1;
	Q.push(0); 
	while (!Q.empty()) {
		vertex = Q.front();
		Q.pop();
		for (int j = 0; j < adj_list[vertex].size(); j++) {
			if (!visit[adj_list[vertex][j].vertex] && adj_list[vertex][j].can_take(ans, i)) {
				Q.push(adj_list[vertex][j].vertex);
				visit[adj_list[vertex][j].vertex] = 1;
				num_connected_vertex++;
			}
		}
	}
	return num_connected_vertex;
}

int main(void)
{
	int T;
	int V, E;
	int vertex;
	int max_weight;
	int ans;
	edge buf;
	vector<vector<edge> > adj_list(100000);
	scanf("%d", &T);
	while (T--) {
		scanf("%d%d", &V, &E);
		for (int i = 0; i < V; i++)
			adj_list[i].clear();
		max_weight = 0;
		for (int i = 0; i < E; i++) {
			scanf("%d %d %d", &vertex, &buf.vertex, &buf.weight);
			buf.vertex--;
			vertex--;
			if (buf.weight > max_weight)
				max_weight = buf.weight;
			adj_list[vertex].push_back(buf);
			swap(vertex, buf.vertex);
			adj_list[vertex].push_back(buf);
		}
		ans = 0;
		for (int i = find_leftmost_bit(max_weight); i >= 0; i--) {
			if (BFS(adj_list, V, ans, i) != V)
				ans += (1 << i);
		}
		printf("%d\n", ans);
	}
	return 0;
}
