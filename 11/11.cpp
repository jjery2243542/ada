#include <stdio.h>
#include <limits.h>

using namespace std;

__uint128_t one = 1;

//count number of bit
int count(__uint128_t a)
{
	return (__builtin_popcountll(a >> 50) + __builtin_popcountll(a & ((one << 50) - 1)));
}

//find the lowest bit
int lowbit(__uint128_t a)
{
	if ((a & ((one << 50) - 1)) == 0)
		return (__builtin_ctzll(a >> 50) + 50);
	else 
		return __builtin_ctzll(a);
}

void find_max_clique(int num_R, __uint128_t P, __uint128_t X, __uint128_t adj[], int n, int &max_set_num)
{
	if (P == 0 && X == 0) {
		if (num_R > max_set_num)
			max_set_num = num_R;
		return;
	}
	int num_P = count(P);
	if (num_P + num_R < max_set_num)
		return;
	int pivot = lowbit(P | X);
	int l;
	while ((P & (~adj[pivot])) > 0 && num_P + num_R > max_set_num) {
		l = lowbit((P & (~adj[pivot])));
		if ((P & (~adj[pivot])) & adj[l] || l == pivot)
			find_max_clique(num_R + 1, P & adj[l], X & adj[l], adj, n, max_set_num);
		P ^= (one << l);
		num_P--;
		X |= (one << l);
	}
	return;
}

int main(void)
{
	int T, n, m;
	int a, b;
	int max_set_num = 0;
	__uint128_t adj[100];
	__uint128_t P, X;
	int num_R;
	scanf("%d", &T);
	while (T--) {
		max_set_num = 0;
		scanf("%d %d", &n, &m);
		//initialize the value to 1111..111
		P = (one << n) - 1;
		num_R = 0;
		X = 0;
		for (int i = 0; i < n; i++) {
			adj[i] = (one << n) - 1;
			adj[i] &= (~(one << i));
		}
		//set the edge
		for (int i = 0; i < m; i++) {
			scanf("%d%d", &a, &b);
			adj[a] &= (~(one << b));
			adj[b] &= (~(one << a));
		}
		find_max_clique(num_R, P, X, adj, n, max_set_num);
		printf("%d\n", max_set_num);
	}
	return 0;
}
