#include <stdio.h>
#include <string.h>
#include <algorithm>

using namespace std;

int table[2002][2002];

void print_lcs(int table_a[][26], int table_b[][26], int a_length, int b_length, int len)
{
	if (len == 0)
		return;
	for (int i = 0; i < 26; i++) {
		if (table[table_a[a_length][i]][table_b[b_length][i]] == len) {
			printf("%c", 'a' + i);//print the last char
			print_lcs(table_a, table_b, table_a[a_length][i] - 1, table_b[b_length][i] - 1, len - 1);//do it recursively
			return;
		}
	}
}

void process(char *a, char *b, int a_length, int b_length)
{
	int table_a[2002][26] = {{0}};
	int table_b[2002][26] = {{0}};
	int buf[26] = {0};
	for (int i = 1; i <= a_length; i++) {
		buf[a[i - 1] - 'a'] = i;
		for (int j = 0; j < 26; j++)
			table_a[i][j] = buf[j];
	}//table[i][j] = the position of last (j + 'a') in string a

	for (int i = 0; i < 26; i++)
		buf[i] = 0;//initialize buf

	for (int i = 1; i <= b_length; i++) {
		buf[b[i - 1] - 'a'] = i;
		for (int j = 0; j < 26; j++)
			table_b[i][j] = buf[j];
	}//table[i][j] = the position of last (j + 'a') in string b
	print_lcs(table_a, table_b, a_length, b_length, table[a_length][b_length]);
	printf("\n");
	return;
}

void lcs(char *a, char *b)
{
	int a_length, b_length;
	a_length = strlen(a);
	b_length = strlen(b);
	for (int i = 0; i < a_length / 2; i++) 
		swap(a[i], a[a_length - i - 1]);
	for (int i = 0; i < b_length / 2; i++) 
		swap(b[i], b[b_length - i - 1]);// reverse the string

	for (int i = 0; i <= a_length; i++)
		table[i][0] = 0;
	for (int i = 0; i <= b_length; i++)
		table[0][i] = 0;// initialize the table

	for (int i = 1; i <= a_length; i++)
		for (int j = 1; j <= b_length; j++) {
			if (a[i - 1] == b[j - 1]) 
				table[i][j] = table[i - 1][j - 1] + 1;
			else {
				if (table[i - 1][j] > table[i][j - 1]) 
					table[i][j] = table[i - 1][j];
				else 
					table[i][j] = table[i][j - 1];
			}
		}// make length table of lcs
	process(a, b, a_length, b_length);
	return;
}

int main(void)
{
	int T;
	scanf("%d", &T);
	while (T--) {
		char a[2014], b[2014];
		scanf("%s", a);
		scanf("%s", b);
		lcs(a, b);
	}
	return 0;
}
