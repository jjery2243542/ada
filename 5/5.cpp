#include <stdio.h>
#include <string.h>

#define MOD_NUM 1000000007
#define MAX_SIZE 15

void preprocess(int power[])
{
	for (int i = 0; i < MAX_SIZE + 1; i++)
		power[i] = (1 << i);//power[i] = 2^i
	return;
}

int fill(char board[][MAX_SIZE + 1], int n, int m, int power[])//n rows, m columns
{
	int dp[n + 1][m][power[m]];
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < m; j++)
			for (int k = 0; k < power[m]; k++)
				dp[i][j][k] = 0;
	dp[n][0][0] = 0;//initialize dp table

	dp[0][0][0] = 1;
	for (int i = 0; i < n; i++) 
		for (int j = 0; j < m; j++)
			for (int k = 0; k < power[m]; k++) {
				if (dp[i][j][k] == 0)
					continue;
				else if (board[i][j] == 'X' || ((k & power[j]) == power[j])) {//filled
					if (j != m - 1) {
						dp[i][j + 1][k & (power[m] - 1 - power[j])] += dp[i][j][k];
						dp[i][j + 1][k & (power[m] - 1 - power[j])] %= MOD_NUM;
					}
					else {
						dp[i + 1][0][k & (power[m] - 1 - power[j])] += dp[i][j][k];
						dp[i + 1][0][k & (power[m] - 1 - power[j])] %= MOD_NUM;
					}
				}
				else {//fill it
					if (j != m - 1) {//fill 1X1
						dp[i][j + 1][k] += dp[i][j][k];
						dp[i][j + 1][k] %= MOD_NUM;
					}
					else {						
						dp[i + 1][0][k] += dp[i][j][k];
						dp[i + 1][0][k] %= MOD_NUM;
					}
					if (board[i + 1][j] != 'X') {//fill 2X1
						if (j != m - 1) {
							dp[i][j + 1][k ^ power[j]] += dp[i][j][k];
							dp[i][j + 1][k ^ power[j]] %= MOD_NUM;
						}
						else {
							dp[i + 1][0][k ^ power[j]] += dp[i][j][k];
							dp[i + 1][0][k ^ power[j]] %= MOD_NUM;
						}
					}
					if (board[i][j + 1] != 'X' && ((k & power[j + 1]) != power[j + 1])) {//fill 1X2
						if (j != m - 2) {
							dp[i][j + 2][k] += dp[i][j][k];
							dp[i][j + 2][k] %= MOD_NUM;
						}
						else {
							dp[i + 1][0][k] += dp[i][j][k];
							dp[i + 1][0][k] %= MOD_NUM;
						}
					}
					if (board[i][j + 1] != 'X' && board[i + 1][j] != 'X' && board[i + 1][j + 1] != 'X' && ((k & power[j + 1]) != power[j + 1])) {//fill 2X2
						if (j != m - 2) {
							dp[i][j + 2][k ^ (power[j] + power[j + 1])] += dp[i][j][k];
							dp[i][j + 2][k ^ (power[j] + power[j + 1])] %= MOD_NUM;
						}
						else {
							dp[i + 1][0][k ^ (power[j] + power[j + 1])] += dp[i][j][k];
							dp[i + 1][0][k ^ (power[j] + power[j + 1])] %= MOD_NUM;
						}
					}
				}
			}
	return dp[n][0][0];
}

int main(void)
{
	int T, n, m;
	int power[MAX_SIZE + 1];
	char board[MAX_SIZE + 1][MAX_SIZE + 1];
	preprocess(power);//make 2_power table
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		scanf("%d", &m);
		for (int i = 0; i < n; i++) {
			scanf("%s", board[i]);
			board[i][m] = 'X';//create blocked boundary
		}
		for (int j = 0; j <= m; j++)
			board[n][j] = 'X';//create blocked boundary
		printf("%d\n", fill(board, n, m, power));
	}
	return 0;
}

