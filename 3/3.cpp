#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

typedef struct query
{
	int l, r;
	long long int v;
}query;// an event

typedef struct mine
{
	double index;
	long long int v;
	bool operator <(const struct mine &b) const
	{
		return index < b.index;
	}// for sort
}mine;// for value change operation

void solve_problem(int begin, int end, vector<long long int> &delta_v, vector<long long int> &player_gold, vector<int> &player_list, vector<vector<int> > &player_mine, vector<query> &event, vector<int> &block_owner, vector<int> &player_goal, vector<int> &answer)
{
	vector<mine> mine_list;
	mine buf;
	int mid;
	mid = (begin + end) / 2;
	for (int i = 0; i < player_list.size(); i++) {
		for (int j = 0; j < player_mine[player_list[i]].size(); j++) {
			buf.index = player_mine[player_list[i]][j];
			buf.v = 0;
			mine_list.push_back(buf);
		}
	}//block in this level

	for (int i = begin; i <= mid; i++) {
		buf.index = event[i].l - 0.5;
		buf.v = event[i].v;
		mine_list.push_back(buf);
		buf.index = event[i].r + 0.5;
		buf.v = -event[i].v;
		mine_list.push_back(buf);
	}//event todo
	sort(mine_list.begin(), mine_list.end());

	long long int rate = 0;
	for (int i = 0; i < player_list.size(); i++)
		delta_v[player_list[i]] = 0;//players' delta value
	for (int i = 0; i < mine_list.size(); i++) {
		if (mine_list[i].v != 0) 
			rate += mine_list[i].v;// rate change
		else
			delta_v[block_owner[(int)mine_list[i].index]] += rate;// add delta value 
	}

	vector<int> player_fail_list;
	vector<int> player_success_list;
	for (int i = 0; i < player_list.size(); i++) {
		if (player_gold[player_list[i]] + delta_v[player_list[i]] >= player_goal[player_list[i]]) {
			answer[player_list[i]] = mid;
			player_success_list.push_back(player_list[i]);
		}//success player
		else {
			player_gold[player_list[i]] += delta_v[player_list[i]];
			player_fail_list.push_back(player_list[i]);
		}//fail player
	}
	if (begin == end)
		return;// end condition
	if (player_success_list.size() != 0)
		solve_problem(begin, mid, delta_v, player_gold, player_success_list, player_mine, event, block_owner, player_goal, answer);
	if (player_fail_list.size() != 0)
		solve_problem(mid + 1, end, delta_v, player_gold, player_fail_list, player_mine, event, block_owner, player_goal, answer);
	return;// recursive call
}

int main(void)
{
	int T;
	int num_player, num_block, num_event;
	scanf("%d", &T);
	for (int i = 0; i < T; i++) {
		scanf("%d%d%d", &num_player, &num_block, &num_event);
		vector<int> player_goal(num_player + 1);
		vector<int> block_owner(num_block + 1);
		vector<int> answer(num_player + 1, -1);
		vector<int> player_list(num_player);
		vector<query> event(num_event + 1);
		vector<long long int> delta_v(num_player + 1);
		vector<long long int> player_gold(num_player + 1, 0);
		vector<vector<int> >player_mine(num_player + 1);
		for (int j = 1; j <= num_player; j++) {
			scanf("%d", &player_goal[j]);
			player_list[j - 1] = j;// create player_list
		}
		int owner;
		for (int j = 1; j <= num_block; j++) {
			scanf("%d", &owner);
			block_owner[j] = owner;
			player_mine[owner].push_back(j);// which blocks belong to the player
		}
		for (int j = 1; j <= num_event; j++) 
			scanf("%d%d%lld", &event[j].l, &event[j].r,	&event[j].v);
		solve_problem( 1, num_event, delta_v, player_gold, player_list, player_mine, event, block_owner, player_goal, answer);
		for (int i = 1; i <= num_player; i++) {
			if (i == num_player)
				printf("%d\n", answer[i]);
			else 
				printf("%d ", answer[i]);
		}
	}
	return 0;
}
