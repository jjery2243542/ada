#include <stdio.h>
#include <algorithm>

using namespace std;

int set_airport(int n, int pos[], int dis)//return number of airport
{
	int *low, *up;
	int count = 0;
	for (int start = 0; start < n; count++) {
		low = upper_bound(pos, pos + n, pos[start] + dis);
		low--;//low is the location of airport
		up = upper_bound(pos, pos + n, *low + dis);
		start = up - pos;//next interval
	}
	return count;
}

int binary_search(int k, int n, int pos[])
{
	int L, R, M;
	int p;
	int ans;
	L = 0;
	R = pos[n - 1] - pos[0];//max distance
	while (L <= R) {
		M = (L + R) / 2;
		p = set_airport(n, pos, M);
		if (p <= k) {
			R = M - 1;
			ans = M;
		}
		else
			L = M + 1;
	}
	return ans;
}

int main(void)
{
	int T, k ,n;
	scanf("%d", &T);
	while (T--) {
		scanf("%d%d", &n, &k);
		int pos[n];
		for (int i = 0; i < n; i++) 
			scanf("%d", &pos[i]);
		sort(pos, pos + n);
		printf("%d\n", binary_search(k, n, pos));
	}
	return 0;
}


