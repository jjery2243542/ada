#include <stdio.h>
#include <queue>

using namespace std;

typedef struct state
{
	int b, len;
	int status;// 0:not visited 1:in the queue 2:visited
	char type;
}state;

state map[100][100][100];

typedef struct point
{
	int x, y, z;
	//0:x - 1, 1:x + 1, 2:y - 1, 3:y + 1, 4:z - 1, 5:z + 1
	point get_next(int dir)
	{
		point next;
		next.x = x;
		next.y = y;
		next.z = z;
		if (dir / 2 == 0) {
			if (dir % 2 == 0) {
				next.x--;
			}
			else 
				next.x++;
		} else if (dir / 2 == 1) {	
			if (dir % 2 == 0) 
				next.y--;
			else 
				next.y++;
		} else if (dir / 2 == 2) {	
			if (dir % 2 == 0) 
				next.z--;
			else 
				next.z++;
		}
		return next;
	}
}point;

int main(void)
{
	int T, n, min_len;
	point recent, next, exit;
	queue<point> Q;
	bool fail = 1;
	scanf("%d", &T);
	while (T--) {
		fail = 1;
		scanf("%d", &n);
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++) 
				for (int k = 0; k < n; k++) {
					map[i][j][k].len = 0;
					map[i][j][k].b = 0;
					map[i][j][k].status = 0;
					scanf(" %c", &map[i][j][k].type);
					if (map[i][j][k].type == 'S') {
						recent.x = i;
						recent.y = j;
						recent.z = k;
					} else if (map[i][j][k].type == 'E') {
						exit.x = i;
						exit.y = j;
						exit.z = k;
					}
				}	
		min_len = 1000001;
		Q.push(recent);
		map[recent.x][recent.y][recent.z].status = 1;
		while (!Q.empty()) {
			recent = Q.front();
			Q.pop();
			map[recent.x][recent.y][recent.z].status = 2;
			if (map[recent.x][recent.y][recent.z].len >= min_len)
				continue;
			for (int i = 0; i < 6; i++) {
				next = recent.get_next(i);
				//can't go
				if (next.x < 0 || next.y < 0 || next.z < 0 || next.x >= n || next.y >= n || next.z >= n || map[next.x][next.y][next.z].type == '#')
					continue;
				if (map[next.x][next.y][next.z].status == 0) {
					map[next.x][next.y][next.z].len = map[recent.x][recent.y][recent.z].len + 1;
					map[next.x][next.y][next.z].b = map[recent.x][recent.y][recent.z].b;
					if (map[next.x][next.y][next.z].type == 'B') 
						map[next.x][next.y][next.z].b++;
					else if (map[next.x][next.y][next.z].type == 'E') {
						fail = 0;
						min_len = map[next.x][next.y][next.z].len;
					} 				
					Q.push(next);
					map[next.x][next.y][next.z].status = 1;
				} else if (map[next.x][next.y][next.z].status == 1) {
					if (map[next.x][next.y][next.z].type == 'B') {
						if (map[recent.x][recent.y][recent.z].b >= map[next.x][next.y][next.z].b)
							map[next.x][next.y][next.z].b = map[recent.x][recent.y][recent.z].b + 1;
					} else {						
						if (map[recent.x][recent.y][recent.z].b > map[next.x][next.y][next.z].b)
							map[next.x][next.y][next.z].b = map[recent.x][recent.y][recent.z].b;
					} 
				}
			}
		}
		if (fail)
			printf("Fail OAQ\n");
		else 
			printf("%d %d\n", min_len, map[exit.x][exit.y][exit.z].b);
		fail = 1;
	}
	return 0;
}
