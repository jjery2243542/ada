#include <stdio.h>

int main(void)
{
	int T, n, i, prev, max_index;
	scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		int a[n], b[n];
		max_index = 0;
		for (i = 0; i < n; i++) {
			scanf("%d", &a[i]);
			b[i] = -2;
			if (a[i] > a[max_index])
				max_index = i;
		}
		b[max_index] = -1;
		for (i = max_index + 1; ; i++) {
			if (i == n)
				i -= n;
			if (i == max_index)
				break;
			if (a[i] == a[max_index])
				b[i] = -1;
			else {
				if (i == 0)
					prev = n - 1;
				else 
					prev = i - 1;
				while (a[prev] <= a[i]) 
					prev = b[prev];
				b[i] = prev;
			}
		}
		printf("%d", b[0] + 1);
		for (i = 1; i < n; i++)
			printf(" %d", b[i] + 1);
		printf("\n");
	}
	return 0;
}
